package config

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type OnlineConf struct {
	Urls []string `yaml:"urls,omitempty"`
	Tls  bool     `yaml:"tls,omitempty"`
}

type LocalConf struct {
	Directories       []string `yaml:"directories"`
	UsePortrait       bool     `yaml:"usePortrait,omitempty"`
	LandscapeCoef     float32  `yaml:"landscapeCoef,omitempty"`
	SetQualityControl bool     `yaml:"setQualityControl,omitempty"`
	MinMps            float32  `yaml:"minMps,omitempty"`
	MaxMps            float32  `yaml:"maxMps,omitempty"`
}

type GlobalConf struct {
	Interval     int  `yaml:"interval,omitempty"`
	UseDirectory bool `yaml:"useDirectory"`
	UseUrls      bool `yaml:"useUrls"`
	Wallmode     int  `yaml:"wallmode,omitempty"`
}

type GenConf struct {
	OnlineConf `yaml:"online"`
	LocalConf  `yaml:"local"`
	GlobalConf `yaml:"global"`
}

type Config struct {
	Conf GenConf `yaml:"conf"`
}

func NewConfig() *Config {
	return &Config{
		Conf: GenConf{
			OnlineConf{
				Urls: []string{"https://pigallery.local/api/gallery/random/{\"type\":100,\"text\":\"\"}"},
				Tls:  false,
			},
			LocalConf{
				Directories:       []string{"~/Pictures"},
				UsePortrait:       false,
				LandscapeCoef:     1.2,
				SetQualityControl: false,
				MinMps:            0.9,
				MaxMps:            35.0,
			},
			GlobalConf{
				Interval:     15,
				UseDirectory: true,
				UseUrls:      false,
				Wallmode:     1,
			},
		},
	}
}

func (c *Config) ReadConf(cnfPath string) {
	buf, err := os.ReadFile(cnfPath)
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(buf, c)
	if err != nil {
		log.Fatal(err)
	}
}
