package logger

import (
	"busterd/wallpaper_slide_show/internal/config"
	"log"
)

func LogConfig(c *config.Config) {
	log.Println("Current settings:")
	log.Println("Global settings:")
	log.Printf("Interval: %d secs.", c.Conf.GlobalConf.Interval)
	log.Printf("Use online images: %t", c.Conf.GlobalConf.UseUrls)
	log.Printf("Use local directories: %t", c.Conf.GlobalConf.UseDirectory)
	log.Printf("Wallpaper mode: %d", c.Conf.GlobalConf.Wallmode)
	log.Println("------------------------------------------")
	log.Println("Online settings:")
	if len(c.Conf.OnlineConf.Urls) > 0 {
		log.Println("URL list:")
		for _, v := range c.Conf.OnlineConf.Urls {
			log.Println(v)
		}
	} else {
		log.Println("URL list not found!")
	}
	log.Printf("Use TLS: %t", c.Conf.OnlineConf.Tls)
	log.Println("------------------------------------------")
	log.Println("Local directories settings:")
	if len(c.Conf.LocalConf.Directories) > 0 {
		log.Println("Directories list:")
		for _, v := range c.Conf.LocalConf.Directories {
			log.Println(v)
		}
	} else {
		log.Println("Directories list not found!")
	}
	log.Printf("Use portrait images: %t", c.Conf.LocalConf.UsePortrait)
	log.Printf("Landscape coefficient: %.2f", c.Conf.LocalConf.LandscapeCoef)
	log.Printf("Use quality control: %t", c.Conf.LocalConf.SetQualityControl)
	log.Printf("Minimum Mps: %.2f", c.Conf.LocalConf.MinMps)
	log.Printf("Maximum Mps: %.2f", c.Conf.LocalConf.MaxMps)
	log.Println("------------------------------------------")
}
