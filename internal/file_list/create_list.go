package filelist

import (
	"io/fs"
	"path/filepath"

	"github.com/gabriel-vasile/mimetype"
)

type NoFiles struct{}

func (e *NoFiles) Error() string {
	return "No image files detected"
}

func GetFileList(directory string) ([]string, error) {
	var list []string
	err := filepath.Walk(directory, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			mime, _ := mimetype.DetectFile(path)
			if mime.Extension() == ".png" || mime.Extension() == ".jpg" {
				list = append(list, path)
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	if len(list) == 0 {
		return nil, &NoFiles{}
	}
	return list, nil
}
