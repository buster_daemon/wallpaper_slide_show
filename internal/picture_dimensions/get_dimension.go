package picturedimensions

import (
	"image"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
)

func GetLandscape(file string, coef float32) bool {
	buf, err := os.Open(file)
	if err != nil {
		log.Println(err)
	}
	defer buf.Close()
	img, _, err := image.Decode(buf)
	if err != nil {
		panic(err)
	}
	if !(float32(img.Bounds().Dy())*coef > float32(img.Bounds().Dx())) {
		log.Printf("%s has landscape orientation.", file)
		return true
	} else {
		log.Printf("%s has portrait orientation.", file)
		return false
	}
}

func GetQuality(file string, min float32, max float32) bool {
	if min > max {
		tmp1 := min
		tmp2 := max
		max = tmp1
		min = tmp2
	}
	buf, err := os.Open(file)
	if err != nil {
		log.Println(err)
	}
	defer buf.Close()
	img, _, err := image.Decode(buf)
	if err != nil {
		panic(err)
	}
	var mps float32 = float32(img.Bounds().Dx() * img.Bounds().Dy())
	if !(mps < min*1_000_000 || mps > max*1_000_000) {
		log.Printf("%s has good quality.", file)
		return true
	} else {
		log.Printf("%s has too poor or too much high quality.", file)
		return false
	}
}
