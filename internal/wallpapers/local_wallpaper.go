package wallpapers

import (
	"busterd/wallpaper_slide_show/internal/config"
	filelist "busterd/wallpaper_slide_show/internal/file_list"
	picturedimensions "busterd/wallpaper_slide_show/internal/picture_dimensions"
	"log"
	"math/rand"
	"time"

	"github.com/reujab/wallpaper"
)

func SetWallpaper(config *config.Config, dir string) {
	for {
		log.Println("Getting file list of directory")
		ls, err := filelist.GetFileList(dir)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Generating seed for choosing a random file.")
		rand.Seed(time.Now().UnixNano())
		log.Println("Choosing a random file:")
		file := ls[rand.Intn(len(ls))]
		log.Println(file)
		if !config.Conf.LocalConf.UsePortrait {
			log.Printf("Getting orientation of %s", file)
			if !picturedimensions.GetLandscape(file, config.Conf.LocalConf.LandscapeCoef) {
				log.Println("Trying again.")
				continue
			}
		}
		if config.Conf.LocalConf.SetQualityControl {
			log.Printf("Getting %s quality", file)
			if !picturedimensions.GetQuality(file, config.Conf.LocalConf.MinMps, config.Conf.LocalConf.MaxMps) {
				log.Println("Trying again.")
				continue
			}
		}
		log.Printf("Setting %s as wallpaper", file)
		wallpaper.SetFromFile(file)
		log.Println("Setting wallpaper mode")
		wallpaper.SetMode(wallpaper.Mode(config.Conf.GlobalConf.Wallmode))
		break
	}
}
