package wallpapers

import (
	"busterd/wallpaper_slide_show/internal/config"
	"crypto/tls"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/reujab/wallpaper"
)

func SetFromURL(config *config.Config) {
	if !config.Conf.OnlineConf.Tls {
		log.Println("Disable TLS check.")
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}
	if len(config.Conf.OnlineConf.Urls) == 1 {
		log.Println("Setting wallpaper from URL.")
		err := wallpaper.SetFromURL(config.Conf.OnlineConf.Urls[0])
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Setting wallpaper mode")
		wallpaper.SetMode(wallpaper.Mode(config.Conf.GlobalConf.Wallmode))
	} else if len(config.Conf.OnlineConf.Urls) > 1 {
		log.Println("Generating seed for choosing URL.")
		rand.Seed(time.Now().UnixNano())
		log.Println("Choosing random URL:")
		url := config.Conf.OnlineConf.Urls[rand.Intn(len(config.Conf.OnlineConf.Urls))]
		log.Println(url)
		log.Printf("Setting %s as wallpaper", url)
		err := wallpaper.SetFromURL(url)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Setting wallpaper mode.")
		wallpaper.SetMode(wallpaper.Mode(config.Conf.GlobalConf.Wallmode))
	} else if len(config.Conf.OnlineConf.Urls) < 1 {
		log.Fatal("No URLs was found.")
	}
}
