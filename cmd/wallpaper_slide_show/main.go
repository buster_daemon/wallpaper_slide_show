package main

import (
	"busterd/wallpaper_slide_show/internal/config"
	"busterd/wallpaper_slide_show/internal/logger"
	"busterd/wallpaper_slide_show/internal/wallpapers"
	"flag"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var cnfPath string

func init() {
	flag.StringVar(&cnfPath, "config", "config.yaml", "Path to config file")
	flag.Parse()
}

func main() {
	cnf := config.NewConfig()
	cnf.ReadConf(cnfPath)
	cnf_p := &cnf
	shutdown_signal := make(chan os.Signal, 1)
	signal.Notify(shutdown_signal, syscall.SIGINT, syscall.SIGABRT, syscall.SIGTERM)
	logger.LogConfig(*cnf_p)
	if !cnf.Conf.GlobalConf.UseDirectory && !cnf.Conf.GlobalConf.UseUrls {
		log.Fatal("Directories mode and URL modes are disabled.")
	}
	if cnf.Conf.LocalConf.LandscapeCoef < 1.0 {
		log.Printf("Too low landscape coefficient: %1.f < 1.0", cnf.Conf.LocalConf.LandscapeCoef)
		cnf.Conf.LocalConf.LandscapeCoef = 1.2
		log.Printf("Setting it to default value: %1.f", cnf.Conf.LocalConf.LandscapeCoef)
	} else if cnf.Conf.LocalConf.LandscapeCoef >= 3.0 {
		log.Printf("Too high landscape coefficient: %1.f >= 3.0", cnf.Conf.LocalConf.LandscapeCoef)
		cnf.Conf.LocalConf.LandscapeCoef = 1.2
		log.Printf("Setting it to default value: %1.f", cnf.Conf.LocalConf.LandscapeCoef)
	}
	for {
		if cnf.Conf.GlobalConf.Interval < 10 {
			log.Printf("Too low time interval: %d < 10", cnf.Conf.GlobalConf.Interval)
			cnf.Conf.GlobalConf.Interval = 10
			log.Printf("Setting it to default value: %d", cnf.Conf.GlobalConf.Interval)
		}
		changingWaaalpaper(*cnf_p)
		time.Sleep(time.Second * time.Duration(cnf.Conf.GlobalConf.Interval))
		if len(shutdown_signal) == cap(shutdown_signal) {
			os.Exit(0)
		}
	}
}

func changingWaaalpaper(config *config.Config) {
	if config.Conf.GlobalConf.UseDirectory && config.Conf.GlobalConf.UseUrls {
		log.Println("Generating seed for choosing between URL and directory.")
		rand.Seed(time.Now().UnixNano())
		chance := rand.Float32()
		if chance > .0 && chance < .5 {
			log.Println("URL mode will be used.")
			wallpapers.SetFromURL(config)
		} else if chance > .49 && chance < 1.1 {
			log.Println("Directory mode will be used.")
			chooseDirectory(config)
		}
	} else if !config.Conf.GlobalConf.UseUrls && config.Conf.GlobalConf.UseDirectory {
		log.Println("Directory mode will be used.")
		chooseDirectory(config)
	} else if config.Conf.GlobalConf.UseUrls && !config.Conf.GlobalConf.UseDirectory {
		log.Println("URL mode will be used.")
		wallpapers.SetFromURL(config)
	} else {
		log.Fatal("Unexpected error occured.")
	}
}

func chooseDirectory(config *config.Config) {
	if len(config.Conf.LocalConf.Directories) == 1 {
		wallpapers.SetWallpaper(config, config.Conf.LocalConf.Directories[0])
	} else if len(config.Conf.LocalConf.Directories) > 1 {
		log.Println("Generating random seed for choosing a random directory.")
		rand.Seed(time.Now().UnixNano())
		log.Println("Choosing a random directory:")
		dir := config.Conf.LocalConf.Directories[rand.Intn(len(config.Conf.LocalConf.Directories))]
		log.Println(dir)
		wallpapers.SetWallpaper(config, dir)
	} else if len(config.Conf.LocalConf.Directories) < 1 {
		log.Fatal("No directories was found.")
	}
}
