SRC = cmd/wallpaper_slide_show/main.go internal/*/*.go
TARGET_OS ?= linux
ARCH ?= amd64
OUTDIR ?= bin
OUTNAME ?= wallpapers
SEPARATOR =
OUTTYPE = 
USEUPX ?= false
UPXFLAGS ?= -9 -v
LDFLAGS ?= -s -w
ifdef ($(OS))
	SEPARATOR = \\
	OUTTYPE = .exe
else
	SEPARATOR = /
ifeq ($(TARGET_OS),windows)	
	OUTTYPE = .exe
else
	OUTTYPE = .o
endif
endif


OUT = $(OUTDIR)$(SEPARATOR)$(OUTNAME)$(OUTTYPE)

all: build

build: $(SRC)
	mkdir -p $(OUTDIR)
	GOOS=$(TARGET_OS) GOARCH=$(ARCH) go build -ldflags="$(LDFLAGS)" -o $(OUT) cmd/wallpaper_slide_show/main.go
ifeq ($(USEUPX),true)
	upx $(UPXFLAGS) $(OUT)
endif	

.PHONY: clean

clean: bin
	-rm -rf $(OUTDIR)
