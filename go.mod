module busterd/wallpaper_slide_show

go 1.19

require github.com/reujab/wallpaper v0.0.0-20210630195606-5f9f655b3740

require (
	golang.org/x/sys v0.2.0 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
)

require (
	github.com/gabriel-vasile/mimetype v1.4.1 // direct
	golang.org/x/net v0.2.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // direct
)
